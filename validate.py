from http import HTTPStatus
import sys
import requests
import yaml
import os
import subprocess


def get_git_revision_hash() -> str:
    return subprocess.check_output(["git", "rev-parse", "HEAD"]).decode("ascii").strip()


# curl --header "Content-Type: application/json" "https://gitlab.example.com/api/v4/projects/:id/ci/lint"
#  --data '{"content": "{ \"image\": \"ruby:2.6\", \"services\": [\"postgres\"], \"before_script\":
# [\"bundle install\", \"bundle exec rake db:create\"], \"variables\": {\"DB_NAME\": \"postgres\"},
#  \"types\": [\"test\", \"deploy\", \"notify\"], \"rspec\": { \"script\": \"rake spec\", \"tags\":
# [\"ruby\", \"postgres\"], \"only\": [\"branches\"]}}"}'

id = 52788462  # CI_PROJECT_ID
url = f"https://gitlab.com/api/v4/projects/{id}/ci/lint"


def main():
    file_paths = [
        val
        for sublist in [
            [os.path.join(i[0], j) for j in i[2]] for i in os.walk(".gitlab/")
        ]
        for val in sublist
        if val.endswith("index.yml")
    ]

    all_passed = True

    for path in file_paths:
        data = {"include": {"local": path}}

        with open("validate-ci.yml", "a") as outfile:
            yaml.dump(data, outfile)

        with open("validate-ci.yml", "r") as outfile:
            outfile.seek(0)

            response = requests.post(
                url=url,
                headers={
                    "Content-Type": "application/json",
                    "PRIVATE-TOKEN": os.environ.get("GITLAB_TOKEN"),
                },
                json={
                    "content": outfile.read(),
                    "dry_run": True,
                    "ref": get_git_revision_hash(),
                },
            )

            if response.status_code == HTTPStatus.OK:
                content = response.json()

                print(f"path: {path}\n valid: {content['valid']}")
                if content["valid"] is False:
                    print(f"path: {path}, errors: {content['errors']}")
                    all_passed = False

            import pdb

            pdb.set_trace()

        with (
            open("validate-ci.yml", "w") as fout,
            open("validate-ci-example.yml") as fin,
        ):
            fout.write(fin.read())

    if all_passed is False:
        sys.exit(1)


if __name__ == "__main__":
    main()
